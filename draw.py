# -*- coding: utf-8 -*-
"""
Created on Tue Aug 29 16:05:09 2017

@author: ecapp
"""

# coding=utf8
import os
import boto3
from datetime import datetime as dt
from StringIO import StringIO
from PIL import (Image, ImageDraw, ImageFont)
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

font_name = 'CeraRoundPro-Regular.otf'
font_path = os.path.join(os.path.dirname("__file__"), font_name)


def draw_center(text1):

    W, H = (382, 200)
    color = (57, 136, 251)

    font_name = 'CeraRoundPro-Regular.otf'
    font_path = os.path.join(os.path.dirname("__file__"), font_name)

    canvas = Image.new('RGB', (W, H), "white")
    draw = ImageDraw.Draw(canvas)
    font = ImageFont.truetype(font_path, 40, encoding="unic")
    w, h = draw.textsize(text1, font=font)
    draw.text(((W-w)/2, (H-h)/2), text1, color, font)

    image_file = StringIO()
    canvas.save(image_file, "PNG")
    image_file.seek(0)
    return image_file


def draw_slide(text1, text2):

    W, H = (382, 200)
    color = (57, 136, 251)

    canvas = Image.new('RGB', (W, H), "white")
    draw = ImageDraw.Draw(canvas)
    font = ImageFont.truetype(font_path, 90, encoding="unic")
    w, h = draw.textsize(text1, font=font)
    draw.text(((W-w)/2, (H-1.5*h)/2), text1, color, font)

    font = ImageFont.truetype(font_path, 40, encoding="unic")
    w, h = draw.textsize(text2, font=font)
    draw.text(((W-w)/2, (H+1.5*h)/2), text2, color, font)

    image_file = StringIO()
    canvas.save(image_file, "PNG")
    image_file.seek(0)
    return image_file


def draw_pie(labels, sizes, colors):
    plt.figure(figsize=(5.09, 2.67))
    matplotlib.rcParams['font.size'] = 18
    plt.pie(sizes, labels=labels, colors=colors,
            autopct='%1.1f%%', startangle=90)

    plt.axis('equal')
    image_file = StringIO()
    plt.savefig(image_file, format='png')
    image_file.seek(0)
    return image_file


def draw_bar(n, keys, values, horizontal=False):
    color = ["#3986fb"]
    plt.figure(figsize=(5.09, 2.67))
    matplotlib.rcParams['font.size'] = 10
    if horizontal:
        plt.barh(range(n), values, align='center', color=color)
        plt.yticks(range(n), keys)
        plt.tick_params(top='off',
                        bottom='off',
                        left='off',
                        right='off',
                        labelleft='on',
                        labelbottom='off')
        plt.gca().invert_yaxis()
        for spine in plt.gca().spines.values():
            spine.set_visible(False)
        plt.subplots_adjust(left=0.35)
    else:
        plt.bar(range(n), values, align='center', color=color)
        plt.xticks(range(n), keys)
        plt.tick_params(top='off',
                        bottom='off',
                        left='off',
                        right='off',
                        labelleft='off',
                        labelbottom='on')
        for spine in plt.gca().spines.values():
            spine.set_visible(False)
        plt.subplots_adjust(left=0.15, bottom=0.2)

    image_file = StringIO()
    plt.savefig(image_file, format='png', bbox_inches='tight')
    image_file.seek(0)
    return image_file


def upload_image(client_page_id, image_file, image_name):
    bucket_name = os.environ['S3_ANALYTICS_BUCKET']
    aws_region = os.environ['AWS_REGION']
    now = dt.now()
    key_name = "/".join([client_page_id,
                         str(now.date()),
                         (image_name +
                          "_" + str(now.hour) +
                          "_" + str(now.minute))])
    bucket = boto3.resource('s3').Bucket(bucket_name)
    bucket.put_object(Key=key_name,
                      Body=image_file,
                      ACL='public-read',
                      ContentType='image/png')
    url = 'https://s3.{}.amazonaws.com/{}/{}'.format(aws_region,
                                                     bucket_name,
                                                     key_name)
    return url


def simple_metric(client_page_id, value, metric_name, file_name):
    if isinstance(value, int):
        if (value >= 10000 and
                value < 1000000):
            value = format(value / float(1000), '.1f') + "K"
            value = value.replace('.0K', 'K')
        elif value >= 1000000:
            value = format(value / float(1000000), '.2f') + "M"
            value = value.replace('.0M', 'M')
    if not isinstance(value, basestring):
        value = str(value)
    image_file = draw_slide(value, metric_name)
    return upload_image(client_page_id, image_file, file_name + '.png')


def no_data(client_page_id, text):
    image_file = draw_center(text)
    return upload_image(client_page_id, image_file, 'no_data.png')


def pie_chart(client_page_id, labels, values, file_name):
    colors = ['lightblue', 'lightpink']
    image_file = draw_pie(labels, values, colors)
    return upload_image(client_page_id, image_file, file_name + '.png')


def vertical_bar(client_page_id, labels, values, file_name):
    image_file = draw_bar(len(labels), labels, values)
    return upload_image(client_page_id, image_file, file_name + '.png')


def horizontal_bar(client_page_id, labels, values, file_name):
    image_file = draw_bar(len(labels), labels, values, horizontal=True)
    return upload_image(client_page_id, image_file, file_name + '.png')
