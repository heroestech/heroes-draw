# -*- coding: utf-8 -*-
import os
import logging
from flask import Flask, request
import draw


""" Initialize Flask app """
app = Flask(__name__)


""" Initialize logger """
logger = logging.getLogger(__name__)
logger.setLevel(logging.ERROR)

""" API Endpoints """


@app.route('/')
def hello():
    return "These is not the Drawer you are looking for"


@app.route('/draw', methods=['POST'])
def draw_pic():
    j = request.json
    print j
    page_id = j['page_id']
    style = j['style']
    if style not in ['simple', 'pie', 'horizontal', 'vertical', 'no_data']:
        return 500
    if style == 'simple':
        return draw.simple_metric(page_id,
                                  j['value'],
                                  j['metric_name'],
                                  j['file_name'])
    elif style == 'pie':
        return draw.pie_chart(page_id,
                              j['labels'],
                              j['values'],
                              j['file_name'])
    elif style == 'vertical':
        return draw.vertical_bar(page_id,
                                 j['labels'],
                                 j['values'],
                                 j['file_name'])
    elif style == 'horizontal':
        return draw.horizontal_bar(page_id,
                                   j['labels'],
                                   j['values'],
                                   j['file_name'])
    elif style == 'no_data':
        return draw.no_data(page_id,
                            j['text'])
    else:
        return 500


if __name__ == "__main__":
    """
    Start webserver
    """
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port, debug=True)
